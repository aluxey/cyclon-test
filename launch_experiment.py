#!/usr/bin/env python3

import os
from python_modules import experiment

# This dictionary contains all the experiment's configuration
experiment_parameters = dict(
    experiment_name="second",
    project_name=os.path.basename(os.getcwd()),

    net=dict(
        name="{project_name}_{experiment_id}",
        addr="13.{experiment_id}.0.0/20",
        gossip_port=10337,
    ),
    dirs=dict(
        root=os.getcwd()+"/",
        output_subdir="output/{experiment_name}/experiment{experiment_id}/"
    ),
    build_script_path="dockerize_go_project.sh",

    image_name="{project_name}_image",
    container=dict(
        name="{project_name}_exp{experiment_id}_{container_id}",
        label="experiment{experiment_id}",
        ip="13.{experiment_id}.0.{container_id}",
        only_stop=False,
        arguments="-lcy={log_cy} -dcy={debug_cy} -cyperiod={period} \
-cygs={gossip_size} -cyvs={view_size} -bsip={bootstrap_ip} -csvdir={output_dir} \
-net={net_addr}"
    ),

    n_devices=20,
    n_experiments=1,
    experiment_duration=60,

    program_parameters=dict(
        log_cy=True,  # Output Cyclon CSV?
        debug_cy=True,  # Print Cyclon debug info to stdout?
        period=1,  # s
        gossip_size=4,
        view_size=8,
    )
)

experiment.build_project(experiment_parameters)

for experiment_id in range(experiment_parameters['n_experiments']):
    experiment.conduct(experiment_id, experiment_parameters)

print("All experiments are done.")
