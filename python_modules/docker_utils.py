#!/usr/bin/env python3

import docker
#import subprocess as sp


# ---------- Setup ----------

client = docker.from_env()
low_level_client = docker.APIClient()


# ---------- Docker handles ----------


def create_network(network):
    global client

    nets_list = client.networks.list(names=[network['name']])
    if len(nets_list) > 0:
        for net in nets_list:
            net.remove()
    print("Creating network {} with address {}.".format(
        network['name'], network['addr']))
    return client.networks.create(
        network['name'], driver="bridge",
        ipam=docker.types.IPAMConfig(
            pool_configs=[docker.types.IPAMPool(subnet=network['addr'])]))


def remove_network(network_name):
    global client

    nets_list = client.networks.list(names=[network_name])
    if len(nets_list) > 0:
        for net in nets_list:
            net.remove()
            print("Removed network {}.".format(network_name))
    return


def remove_containers(image_name=None, container_label=None, only_stop=False):
    global client

    if image_name is None:
        raise ValueError("Please specify at least an ancestor image_name "
                         "to remove containers.")

    cont_list = []
    if container_label is None:
        cont_list = client.containers.list(filters=dict(ancestor=image_name),
                                           all=True)
        if len(cont_list) == 0:
            return
        print("Removing {} existing containers having "
              "ancestor_image={}...".format(len(cont_list), image_name))
    else:
        print("[remove_containers] Trying to remove label {}...".format(
            container_label))
        cont_list = client.containers.list(
            filters=dict(label=container_label), all=True)
        if len(cont_list) == 0:
            return
        print("Removing {} existing containers having "
              "ancestor_image={} and label={}...".format(
                  len(cont_list), image_name, container_label))

    # Actually stopping/removing containers
    for cont in cont_list:
        if cont.status == "paused":
            # print("Unpausing {} before stopping or removing it.".format(
            #     cont.name))
            cont.unpause()
        if only_stop:
            cont.stop()
        else:
            cont.remove(force=True)
    print("Successfully removed containers.")


def start_container(cont_name, cont_args, labels, network, image_name,
                    volumes=None, cont_ip=None):
    global low_level_client, client

    # Register us to net_name with predefined IP cont_ip
    network_config = {}
    if cont_ip is not None:
        network_config = low_level_client.create_networking_config({
            network['name']: low_level_client.create_endpoint_config(
                ipv4_address=cont_ip)
        })

    # Create volumes configuration from the volumes dict
    volumes_config = {}
    volumes_cont_list = []
    if volumes is not None:
        for _, vol in volumes.items():
            volumes_config[vol['host_dir']] = {
                'bind': vol['cont_dir'],
                'mode': vol['mode']
            }
            volumes_cont_list.append(vol['cont_dir'])

    host_config = low_level_client.create_host_config(binds=volumes_config)

    cont = low_level_client.create_container(
        image_name, cont_args, detach=True, volumes=volumes_cont_list,
        name=cont_name, labels=labels, networking_config=network_config,
        host_config=host_config)
    low_level_client.start(cont)

    cont_id = cont['Id'][:12]
    cont_ip = client.containers.get(cont_id)\
        .attrs['NetworkSettings']['Networks'][network['name']]['IPAddress']

    return cont_id, cont_ip


def get_container(cont_id):
    containers_list = client.containers.list(filters={'id': cont_id})
    if len(containers_list) == 0:
        return None
    if len(containers_list) > 1:
        raise NameError(
            "Too many containers with ID '{}': {}".format(
                cont_id, containers_list))
    return containers_list[0]


def container_is_running(cont_id):
    return (get_container(cont_id) != None)


def container_is_paused(cont_id=None, cont=None):
    if cont == None:
        cont = get_container(cont_id)
        if cont == None:
            raise ConnectionError(
                "Container '{}' is not running.".format(cont_id))

    return (cont.status == "paused")


def pause_container(cont_id):
    cont = get_container(cont_id)
    if cont == None:
        raise ConnectionError(
            "Container '{}' is not running.".format(cont_id))

    if not container_is_paused(cont=cont):
        cont.pause()


def unpause_container(cont_id):
    cont = get_container(cont_id)
    if cont == None:
        raise ConnectionError(
            "Container '{}' is not running.".format(cont_id))

    if container_is_paused(cont=cont):
        cont.unpause()
