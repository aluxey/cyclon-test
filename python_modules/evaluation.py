#!/usr/bin/env python3

from . import constants
import pandas as pd
import os
import numpy as np

TIMESTAMP = "timestamp"
ID = "id"
VIEW = "view"


# experiments_path: path to experiments output (don't forget trailing slash)
def fetch_results(experiments_path):
    parameters_df = pd.DataFrame()
    cyclon_df = pd.DataFrame()

    subdirs = os.listdir(experiments_path)
    for subdir in subdirs:
        experiment_path = experiments_path+subdir+"/"

        # Get the parameters of this experiment
        these_parameters = pd.read_csv(experiment_path+constants.PARAMETERS_FN,
                                       dtype=np.int)
        these_parameters['path'] = experiment_path
        parameters_df = parameters_df.append(
            these_parameters, ignore_index=True)

        # And the cyclon CSVs
        for entry in os.scandir(experiment_path):
            if entry.name.startswith("cyclon"):
                df = pd.read_csv(experiment_path+entry.name,
                                 parse_dates=[TIMESTAMP])
                df['experiment_id'] = int(these_parameters['experiment_id'])
                cyclon_df = cyclon_df.append(df, ignore_index=True)

    parameters_df.sort_values('experiment_id', inplace=True)
    cyclon_df[VIEW] = cyclon_df[VIEW].apply(splitList)
    cyclon_df.sort_values(TIMESTAMP, inplace=True)

    return parameters_df, cyclon_df


def splitList(x):
    if pd.isnull(x) or x == '' or x == 'nan':
        return []
    return x.split('|')
