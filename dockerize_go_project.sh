#!/bin/bash

# Author: Adrien Luxey
# Date: May 2018
#
# This script builds a Golang project (located at an arbitrary location)
# into a lightweight Docker image.
# It does so by first symlinking the code path into the GOPATH,
# using Golang dep to vendor the project (i.e. static linking),
# and finally using Docker's golang/alpine 
# to 'go build' a lightweight compiled image.

project_name=$(basename `pwd`)
if [ $# -ne 1 ]; then
	echo "Usage: $0 [project-name]"
	echo "    project-name: your project's name will be its basedir in your"
	echo "                  \$GOPATH and a part of the output Docker image."
	echo "                  Defaults to using the current dir name (${project_name})."
	echo ""
	echo "This script builds a Golang project (located at an arbitrary location)"
	echo "into a lightweight Docker image called \${project-name}_image."
	echo ""

	read -p "Do you want to use '${project_name}' as your project name? (Y/n) " yn
	if [ "$yn" == "n" ] || [ "$yn" == "N" ]; then
		exit 0
	fi
else 
	project_name=$1
fi

project_path=""

if [ -z ${GOPATH+x} ]; then
	echo >&2 "Error: \$GOPATH variable is not set."
	exit 3
else
	echo -e "\n\$GOPATH exists at $GOPATH, cool."

	# Golang dep needs the project to reside in the GOPATH
	# This is why we make a symlink from the code_path to $GOPATH/src/
	project_path="${GOPATH}/src/${project_name}"
	code_path=""
	if [ ! -d $project_path ]; then
		echo "The project path (${project_path}) does not exist, we need to create it."

		while [[ ! -d $code_path ]]; do 
			current_path=$(pwd)
			read -p "Where is the code currently located (leave empty for: ${current_path}) ?" code_path

			if [[ $code_path == "" ]]; then
				code_path=$current_path
			fi
		done

		ln -s $code_path $project_path
		echo -e "Created a symlink from ${code_path} to ${project_path}.\n"
	else 
		echo -e "Project path exists at $project_path, cool.\n"
	fi
fi

# I am really sorry but we use Golang dep to vendor our dependencies
command -v dep > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo >&2 "Error: We need dep to install the project's dependencies, please install it:"
	echo >&2 "\thttps://golang.github.io/dep/docs/installation.html"
	echo >&2 "(Also ensure ${GOPATH}/bin is in your \$PATH)."
	exit 4
fi

current_path=$(pwd)
cd $project_path
if [ ! -d "${project_path}/vendor" ]; then
	# godep will add external dependencies to vendor/ subdirectory in project path
	# which will make it possible to build project from docker without problems
	echo "Calling 'dep init' on ${project_path} to init vendoring... "
	dep init # Fetch dependencies online
	# dep init -gopath # Fetch dependencies inside GOPATH
	if [ $? -ne 0 ]; then
		echo >&2 "Dep failed initialization."
		exit 5
	fi
	echo -e "Success\n"
else
	echo "Calling 'dep ensure' on ${project_path} to vendor dependencies... "
	dep ensure
	if [ $? -ne 0 ]; then
		echo >&2 "Dep failed vendoring the project."
		exit 6
	fi
	echo -e "Success\n"
fi
cd $current_path

echo "Writing Dockerfile..."
cat > Dockerfile <<- EOM
	FROM library/alpine

	ADD ${project_name} /

	ENTRYPOINT [ "/${project_name}" ]

	CMD [ "--help" ]
EOM


echo "Building project..."
docker run --rm -v "${project_path}:/go/src/${project_name}" -w /go/src/${project_name} \
golang:alpine go build ${project_name}

if [ $? -ne 0 ]; then
	echo >&2 "Error building project"
	exit 1
fi
docker build -t ${project_name}_image ${project_path}
echo "Successfully built project, bye!"

exit 0

