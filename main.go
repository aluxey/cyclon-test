package main

import (
	"flag"
	"gitlab.inria.fr/aluxey/csvlog"
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/gossip/cyclon"
	"gitlab.inria.fr/aluxey/transport"
	"log"
	"math/rand"
	"time"
)

func init() {
	// rand is deterministic until you seed it
	rand.Seed(time.Now().UnixNano())
}

func main() {
	var err error

	// Parse command line parameters
	var cyLogToCsv, cyDebug bool
	var netAddr, bsIP, csvDir string
	var cyPeriod, cyGossipSize, cyViewSize int
	flag.BoolVar(&cyLogToCsv, "lcy", false,
		"Log Cyclon data to CSV?")
	flag.BoolVar(&cyDebug, "dcy", false,
		"Print Cyclon debug information?")
	flag.IntVar(&cyPeriod, "cyperiod", 1,
		"Time period of a Cyclon cycle in seconds")
	flag.IntVar(&cyGossipSize, "cygs", 1,
		"Max size of Cyclon's exchanged views")
	flag.IntVar(&cyViewSize, "cyvs", 1,
		"Max size of Cyclon's internal view")
	flag.StringVar(&bsIP, "bsip", "",
		"IP of an existing peer, for gossip bootstrap")
	flag.StringVar(&csvDir, "csvdir", "",
		"Directory where to save CSV output")
	flag.StringVar(&netAddr, "net", "",
		"Address of the network in CIDR format (x.y.z.t/m)")
	flag.Parse()

	// Set the CSV output location
	if err = csvlog.SetOutputLocation(csvDir); err != nil {
		log.Fatalf("Could not create the CSV output directory: %v\n", err)
	}

	// Create a descriptor for myself
	myIP := ""
	if myIP, err = transport.GetMyIP(netAddr); err != nil {
		log.Fatalf("Error getting my address: %v\n", err)
	}
	myself := gossip.NewDescriptor(myIP)
	log.Printf("I am: %+v\n\n", myself)

	// Create a network manager
	// if UDP, beware of MTU overflow!
	var netManager transport.Manager
	if netManager, err = transport.NewUDPManager(myIP); err != nil {
		log.Fatalf("Error initialising UDP network manager: %v\n", err)
	}

	// Create a bootstrapper for gossip algorithm
	bs := gossip.NewListBootstrap()
	if bsIP != "" {
		bs.Add(bsIP)
	}

	var cy *cyclon.Cyclon
	if cy, err = cyclon.New(myself, bs, netManager,
		cyPeriod, cyViewSize, cyGossipSize, cyLogToCsv, cyDebug); err != nil {
		log.Fatalf("Cyclon creation failed: %v\n", err)
	}

	// Will play forever
	cy.Loop()
}
