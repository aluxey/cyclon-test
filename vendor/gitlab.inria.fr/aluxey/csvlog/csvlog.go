package csvlog

import (
	"encoding/csv"
	"fmt"
	"os"
)

// TODO: add automated timestamp column? Maybe optional?

var outputLocation = ""

func SetOutputLocation(path string) error {
	if err := os.MkdirAll(path, 0755); err != nil {
		return err
	}

	outputLocation = path
	return nil
}

type CSVLog struct {
	out     *csv.Writer
	linesCh chan []string

	nCols int
}

func New(filename string, header []string) (*CSVLog, error) {
	l := new(CSVLog)

	filepath := filename
	if outputLocation != "" {
		filepath = fmt.Sprintf("%s/%s", outputLocation, filename)
	}

	if f, err := os.Create(filepath); err != nil {
		return nil, err
	} else {
		l.out = csv.NewWriter(f)
		if err = l.out.Write(header); err != nil {
			return nil, err
		}
	}

	l.linesCh = make(chan []string)
	l.nCols = len(header)

	go l.loop()

	return l, nil
}

func (l *CSVLog) Write(line []string) error {
	if l.nCols != len(line) {
		return fmt.Errorf("CSV line has invalid number of columns")
	}

	l.linesCh <- line
	return nil
}

func (l *CSVLog) loop() {
	for line := range l.linesCh {
		if err := l.out.Write(line); err == nil {
			l.out.Flush()
		}
	}
}
