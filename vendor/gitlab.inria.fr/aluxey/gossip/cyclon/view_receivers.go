package cyclon

import "gitlab.inria.fr/aluxey/gossip"

/*
ViewReceivers implement the ReceiveViewUpdate(*View) method and register to Cyclon.

Cyclon pushes its view every time it is updated, by calling its updateViewReceivers(*View) method.
The calling function needs to provide the previous view as a parameter for comparison before sending updates.
*/

type ViewReceiver interface {
	// Send a copy!
	ReceiveViewUpdate(gossip.Viewer)
}

func (cy *Cyclon) AddViewReceiver(vr ViewReceiver) {
	// cy.viewReceivers modifications must be done inside the mutex
	cy.viewReceivers.Lock()
	defer cy.viewReceivers.Unlock()

	cy.viewReceivers.m[vr] = struct{}{}
}

// This may be useful in the future, but not now
//func (cy *Cyclon) RemoveViewReceiver(vr ViewReceiver) {}

func (cy *Cyclon) updateViewReceivers(prevView gossip.Viewer) {
	// Must be called from within the mutex!

	if prevView.Equals(cy.V) {
		return
	}

	cy.debug.Printf("[updateViewReceivers] The view has changed: " +
		"we update the ViewReceivers.\n")

	cy.viewReceivers.RLock()
	for vr := range cy.viewReceivers.m {
		go vr.ReceiveViewUpdate(cy.V.Copy())
	}
	cy.viewReceivers.RUnlock()
}
