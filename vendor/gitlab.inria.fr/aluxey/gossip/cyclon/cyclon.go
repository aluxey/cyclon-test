package cyclon

import (
	"encoding/gob"
	"fmt"
	"gitlab.inria.fr/aluxey/csvlog"
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/transport"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"
)

const (
	DEBUG         = false
	CHAN_BUF_SIZE = 5
)

type Payload struct {
	Sender *Descriptor
	V      *View
}

func init() {
	gob.Register(Payload{})
	// Do not forget the &
	// *View is a Viewer, not View
	gob.Register(&View{})
	gob.Register(&Descriptor{})
}

// Observer pattern to track answers
type Cyclon struct {
	// Me and the others
	myself *Descriptor
	V      *View

	// Algorithm's parameters
	viewSize, gossipSize      int
	period, connectionTimeout time.Duration

	// Network
	tm           transport.Manager
	reqCh, ansCh <-chan transport.Message

	answerObservers struct {
		sync.RWMutex
		m map[string]chan Payload
	}

	// We use a map to avoid duplicate entries
	viewReceivers struct {
		sync.RWMutex
		m map[ViewReceiver]struct{}
	}

	bs gossip.Bootstrapper

	// running bool
	// stopCh chan struct{}

	// Logging
	csv         *csvlog.CSVLog
	info, debug *log.Logger

	// Iteration counters
	cntActive, cntPassive int
	// Synchronization
	mutex sync.RWMutex
}

func New(myself gossip.Describer, bs gossip.Bootstrapper, tm transport.Manager,
	period, viewSize, gossipSize int, logToCsv, debug bool) (*Cyclon, error) {

	// First kill Cyclon if it existed
	cy := new(Cyclon)

	// Save cy.myself as a *cyclon.Descriptor depending on the parameter type
	switch m := myself.(type) {
	case *Descriptor:
		cy.myself = m
	case *gossip.Descriptor:
		cy.myself = NewDescriptorFromGossipDescriptor(m)
	default:
		return nil, fmt.Errorf(
			"Parameter myself (%s) of unexpected type %T.",
			myself, myself)
	}

	//cy.running = false
	cy.bs = bs
	cy.cntActive, cy.cntPassive = 0, 0
	cy.connectionTimeout = cy.period
	cy.gossipSize = gossipSize
	cy.period = time.Duration(period) * time.Second
	cy.tm = tm
	cy.V, _ = NewView()
	cy.viewSize = viewSize

	// People register if they want to receive Cy's view when updated
	cy.viewReceivers.m = make(map[ViewReceiver]struct{})

	// ----------- Network -----------
	reqCh := make(chan transport.Message)
	ansCh := make(chan transport.Message)
	// Register chans on the transport Manager
	cy.tm.RegisterObserver(transport.MessageType("CYCLON_REQ"), reqCh)
	cy.tm.RegisterObserver(transport.MessageType("CYCLON_ANS"), ansCh)
	// Keep a reference
	cy.reqCh = reqCh
	cy.ansCh = ansCh

	// Internal dispatcher
	cy.answerObservers.m = make(map[string]chan Payload)

	// ----------- Logging -----------
	// To console
	cy.info = log.New(os.Stdout, "CYCLON: ", log.Ltime|log.Lshortfile)
	if debug {
		cy.debug = log.New(os.Stdout, "CYCLON: ", log.Ltime|log.Lshortfile)
	} else {
		cy.debug = log.New(ioutil.Discard, "CYCLON: ", log.Ltime|log.Lshortfile)
	}
	// To CSV
	if logToCsv {
		now := time.Now()
		filename := fmt.Sprintf("cyclon-%v_%02d-%02d-%02d_%02dh%02d.csv",
			cy.myself.IP(),
			now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
		header := []string{"id", "timestamp", "view"}

		var err error
		if cy.csv, err = csvlog.New(filename, header); err != nil {
			return nil, fmt.Errorf("Failed creating Cyclon CSVLog: %v", err)
		}
	}

	cy.info.Printf(
		"Cyclon started with viewSize=%d, gossipSize=%d and a period of %ds.\n",
		viewSize, gossipSize, period)

	return cy, nil
}

func (cy *Cyclon) Loop() {
	cy.debug.Printf("Loop started.\n")

	// cy.StopCh = make(chan struct{})
	// cy.running = true

	ticker := time.NewTicker(cy.period)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			go cy.active()
		case mess := <-cy.reqCh:
			go cy.passive(mess)
		case mess := <-cy.ansCh:
			go cy.dispatchAnswers(mess)
			// case <-cy.StopCh:
			// 	break
		}
	}
}

// func (cy *Cyclon) Stop() {
// 	// handles closing cy.StopCh
// 	cy.GossipAlgorithm.Stop()
// 	cy.running = false
// }

func (cy *Cyclon) active() {
	// We lock mutex while we use V or myself
	cy.mutex.Lock()
	// We create a local variable in case the counter gets incremented while out of mutex
	cnt := cy.cntActive
	cy.cntActive++

	// 1 - Increase by one the age of all neighbors
	cy.V.IncrementAge()

	// 2 - Select neighbor Q with the highest age among all neighbors ...
	var q *Descriptor
	if cy.V.Empty() {
		cy.debug.Printf(
			"[active #%v] Our view is empty, we query the bootstrap server.\n",
			cnt)
		if IP, err := cy.bs.GetIP(); err != nil {
			cy.info.Printf(
				"[active #%v] The bootstraper failed providing an IP: %v\n",
				cnt, err)
			cy.mutex.Unlock()
			return
		} else {
			cy.debug.Printf(
				"[active #%v] The bootstraper sent us the following IP: %v\n",
				cnt, IP)
			// q won't be added to Cyclon's view
			// We use the BaseDescriptor to create a gossip.Descriptor without other knowledge than the Addr
			q = NewDescriptor(IP)
		}
	} else {
		q = cy.V.Oldest()
		cy.V.Remove(q)
	}

	// 2 - Select [...] l-1 other random neighbors
	buf_snd := getViewSubset(cy.V, cy.gossipSize-1)
	// 3 - Replace Q's entry w/ a new entry of age 0 and with P's address
	// That is: add my descriptor (has age of 0) to the view we will send
	buf_snd.Remove(q)
	buf_snd.Add(cy.myself)

	cy.debug.Printf(
		"[active #%v] Sending following CYCLON_REQ to %s:\n%v\n",
		cnt, q, buf_snd)

	// 4 - Send the updated subset to Q
	go transport.EncodeAndSend(
		Payload{cy.myself, buf_snd.Copy().(*View)},
		q.IP(), cy.tm, transport.MessageType("CYCLON_REQ"))

	cy.mutex.Unlock()

	timeout := time.After(cy.connectionTimeout)
	select {
	case <-timeout:
		cy.info.Printf(
			"[active #%v] %v timed out: removing it from view.\n",
			cnt, q.ID())

		cy.mutex.Lock()
		cy.V.Remove(q)
		cy.mutex.Unlock()
	// 5 - Receive from Q a subset of no more than l of its own entries
	case payload := <-cy.getChannel(q.ID()):
		cy.debug.Printf(
			"[active #%v] Received following CYCLON_ANS from %s:\n%v\n\n",
			cnt, payload.Sender.ID(), payload.V)

		cy.mergeWithView(payload, buf_snd, q)
	}
}

func (cy *Cyclon) passive(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("CYCLON_REQ"))

	cy.mutex.Lock()
	cy.cntPassive++

	if x == nil {
		cy.info.Printf("[passive #%v] The received payload is nil",
			cy.cntPassive)
		cy.mutex.Unlock()
		return
	}
	payload := x.(Payload)

	cy.debug.Printf("[passive #%v] Received following CYCLON_REQ from %s:\n%v\n",
		cy.cntPassive, payload.Sender, payload.V)

	// Upon reception, Q selects l of its own neighbors to send back to P
	buf_snd := getViewSubset(cy.V, cy.gossipSize)
	//buf_snd.Add(cy.myself)
	// We remove P from the view (in paper at step 6, but doing so here
	// removes useless transport overload)
	buf_snd.Remove(payload.Sender)

	cy.debug.Printf("[passive #%v] Sending following CYCLON_ANS to %s:\n%v\n\n",
		cy.cntPassive, payload.Sender, buf_snd)

	go transport.EncodeAndSend(
		Payload{cy.myself, buf_snd.Copy().(*View)},
		payload.Sender.IP(), cy.tm, transport.MessageType("CYCLON_ANS"))

	cy.mutex.Unlock()

	cy.mergeWithView(payload, buf_snd, nil)
}

func (cy *Cyclon) dispatchAnswers(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("CYCLON_ANS"))
	if x == nil {
		cy.info.Printf(
			"[dispatchAnswers] Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	ID := payload.Sender.ID()

	cy.answerObservers.RLock()
	if cy.answerObservers.m[ID] == nil {
		cy.info.Printf(
			"[dispatchAnswers] Received answer from unexpected sender %v, skipping.\n",
			ID)
		cy.answerObservers.RUnlock()
		return
	}

	// non-blocking -channel has buffer of CHAN_BUF_SIZE
	cy.answerObservers.m[ID] <- payload
	cy.answerObservers.RUnlock()
}

func (cy *Cyclon) getChannel(ID string) <-chan Payload {
	cy.answerObservers.Lock()
	defer cy.answerObservers.Unlock()

	if cy.answerObservers.m[ID] == nil {
		cy.debug.Printf("[getChannel] Registering new channel for %v.\n", ID)
		cy.answerObservers.m[ID] = make(chan Payload, CHAN_BUF_SIZE)
	}

	return cy.answerObservers.m[ID]
}
