package cyclon

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
)

// View implements gossip.View
// It gives descriptor's age operation in addition to gossip.View functionality
type View struct {
	gossip.View
}

func NewView(items ...gossip.Describer) (*View, error) {
	v := new(View)
	v.View = make(map[string]gossip.Describer)
	if err := v.Add(items...); err != nil {
		return nil, err
	}
	return v, nil
}

// ----------------- Age functionality -----------------
func (v *View) IncrementAge() {
	for _, item := range v.View {
		if cyItem, ok := item.(*Descriptor); !ok {
			panic(fmt.Sprintf(
				"cyclon.View contains an item that is not an *cyclon.Descriptor: %v of type %T",
				item, item))
		} else {
			cyItem.IncrementAge()
		}

	}
}
func (v *View) Oldest() *Descriptor {
	oldestAge := 0
	oldestKey := ""
	for key, item := range v.View {
		if cyItem, ok := item.(*Descriptor); !ok {
			panic(fmt.Sprintf(
				"cyclon.View contains an item that is not a *cyclon.Descriptor: %v of type %T",
				item, item))
		} else {
			if cyItem.Age() > oldestAge {
				oldestKey = key
				oldestAge = cyItem.Age()
			}
		}
	}
	return v.View[oldestKey].Copy().(*Descriptor)
}
func (v *View) ResetAge() {
	for key := range v.View {
		if cyItem, ok := v.View[key].(*Descriptor); !ok {
			panic(fmt.Sprintf(
				"cyclon.View contains an item that is not a *cyclon.Descriptor: %v of type %T",
				cyItem, cyItem))
		} else {
			cyItem.ResetAge()
		}

	}
}

// ----------------- Overrides from gossip.View -----------------
func UnionView(views ...gossip.Viewer) (*View, error) {
	v, _ := NewView()
	for _, view := range views {
		if view == nil {
			continue
		}
		if err := v.Add(view.List()...); err != nil {
			return nil, err
		}
	}

	return v, nil
}

func (v *View) Add(items ...gossip.Describer) error {
	var ok bool
	for _, item := range items {
		if item == nil {
			continue
		}

		// Type assertion/cast for parameter item
		var toAdd *Descriptor
		if toAdd, ok = item.(*Descriptor); !ok {
			toAdd = &Descriptor{
				*item.(*gossip.Descriptor),
				0}
			// return fmt.Errorf(
			// 	"Value %v of type %T should be of type *cyclon.Descriptor",
			// 	item, item)
		}

		// When duplicates...
		var d gossip.Describer
		if d, ok = v.View[toAdd.ID()]; ok {
			// Type assertion/cast for internal item
			var existing *Descriptor
			if existing, ok = d.(*Descriptor); !ok {
				panic(fmt.Sprintf(
					"Value %v of type %T should be of type *cyclon.Descriptor",
					existing, existing))
			}

			// ...keep younger
			if existing.Younger(toAdd) {
				continue
			}
		}
		v.View[toAdd.ID()] = toAdd.Copy()
	}
	return nil
}

// Overriding gossip.View to keep cyclon functionality when copying
func (v *View) Copy() gossip.Viewer {
	v2, _ := NewView(v.List()...)
	return v2
}

// Must be overridden to use cyclon.View.Add in cyclon.View.Merge
// func (v *View) Merge(views ...gossip.Viewer) error {
// 	for _, view := range views {
// 		if err := v.Add(view.List()...); err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }
func (v *View) String() string {
	ret := fmt.Sprintf("View(len:%v){", v.Len())
	for _, item := range v.View {
		if _, ok := item.(*Descriptor); !ok {
			panic(fmt.Sprintf(
				"cyclon.View contains an item that is not a *cyclon.Descriptor: %v of type %T",
				item, item))
		}
		ret += fmt.Sprintf("\n\t%s,", item.(*Descriptor))
	}
	ret += "\n}"
	return ret
}
