package cyclon

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
)

// Descriptor implements the gossip.Describer interface
// It gives descriptor's age operations in addition to gossip.Descriptor functionality
type Descriptor struct {
	gossip.Descriptor

	MyAge int
}

func NewDescriptor(IP string) (d *Descriptor) {
	return &Descriptor{*gossip.NewDescriptor(IP), 0}
}

func NewDescriptorFromGossipDescriptor(gd *gossip.Descriptor) (d *Descriptor) {
	return &Descriptor{*gd, 0}
}

func (d *Descriptor) Age() int { return d.MyAge }
func (d *Descriptor) Copy() gossip.Describer {
	return &Descriptor{gossip.Descriptor{d.MyID, d.MyIP}, d.MyAge}
}
func (d *Descriptor) IncrementAge() { d.MyAge++ }
func (d *Descriptor) ResetAge()     { d.MyAge = 0 }
func (d *Descriptor) String() string {
	return fmt.Sprintf("cyclon.Descriptor(%p){ID: %s, IP: %s, Age: %d}",
		d, d.MyID, d.MyIP, d.MyAge)
}
func (d *Descriptor) Younger(d1 *Descriptor) bool { return d.MyAge < d1.Age() }
