package gossip

import (
	"encoding/gob"
)

func init() {
	gob.Register(&Descriptor{})
	gob.Register(&View{})
}

// Describer is a network node profile
// A Describer is identified by its ID
type Describer interface {
	Copy() Describer
	Equals(Describer) bool
	ID() string
	IP() string
	String() string
}

// Viewer is a set handling Describers
type Viewer interface {
	Add(items ...Describer) error
	Copy() Viewer
	Diff(Viewer)
	Empty() bool
	Equals(v2 Viewer) bool
	Has(item Describer) bool
	IDs() string
	Len() int
	List() []Describer         // Returns a copy
	Map() map[string]Describer // Returns a copy
	//Merge(views ...Viewer) error
	Remove(items ...Describer)
	String() string
}
