package gossip

import (
	"fmt"
	"math/rand"
)

const (
	CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	ID_SIZE    = 10
)

func RandomID() (b string) {
	for i := 0; i < ID_SIZE; i++ {
		b += string(CHARACTERS[rand.Int63()%int64(len(CHARACTERS))])
	}
	return
}

// Descriptor implements the Describer interface
type Descriptor struct {
	MyID string
	MyIP string
}

func NewDescriptor(IP string) *Descriptor {
	return &Descriptor{
		MyID: RandomID(),
		MyIP: IP,
	}
}
func (d *Descriptor) Copy() Describer          { return &Descriptor{d.MyID, d.MyIP} }
func (d *Descriptor) Equals(d1 Describer) bool { return d.MyID == d1.ID() }
func (d *Descriptor) ID() string               { return d.MyID }
func (d *Descriptor) IP() string               { return d.MyIP }
func (d *Descriptor) String() string {
	return fmt.Sprintf("gossip.Descriptor(%p){ID: %s, IP: %s}",
		d, d.MyID, d.MyIP)
}
