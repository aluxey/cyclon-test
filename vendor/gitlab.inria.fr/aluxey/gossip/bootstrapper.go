package gossip

import (
	"fmt"
	"math/rand"
)

// A Bootstrapper gives an IP to initialize a gossip algorithm
type Bootstrapper interface {
	GetIP() (string, error)
}

// ListBootstrap implements Bootstrapper
// It is built with a list of IPs, and returns a random IP from this list
type ListBootstrap struct {
	IPs []string
}

func NewListBootstrap(IPs ...string) *ListBootstrap {
	b := new(ListBootstrap)
	b.Add(IPs...)

	return b
}

func (b *ListBootstrap) GetIP() (string, error) {
	if len(b.IPs) == 0 {
		return "", fmt.Errorf("ListBootstrap's IPs list is empty.")
	}
	return b.IPs[rand.Intn(len(b.IPs))], nil
}

func (b *ListBootstrap) Add(IPs ...string) {
	b.IPs = append(b.IPs, IPs...)
}
