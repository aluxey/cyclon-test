package transport

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"net"
	"os"
)

// ------------ Send & Receive messages ------------

func EncodeAndSend(x interface{}, recipientIP string,
	nm Manager, messType MessageType) {

	// Create a buffer to encode the payload into
	var encodedPayload bytes.Buffer
	enc := gob.NewEncoder(&encodedPayload)
	if err := enc.Encode(&x); err != nil {
		fmt.Fprintf(os.Stderr, "[EncodeAndSend] Error while encoding payload: %v\n", err)
		return
	}

	nm.Send(Message{messType, encodedPayload.Bytes()}, recipientIP)
}

func Decode(mess Message, expectedType MessageType) interface{} {
	// Little assertion to be sure we did things right
	if mess.Type != expectedType {
		fmt.Fprintf(os.Stderr,
			"[Decode] Wrong datagram type received: expected %v, got %v .\n",
			expectedType, mess.Type)
		return nil
	}

	var decoded interface{}
	decoderBuf := bytes.NewBuffer(mess.Payload)
	dec := gob.NewDecoder(decoderBuf)
	if err := dec.Decode(&decoded); err != nil {
		fmt.Fprintf(os.Stderr, "[Decode] Error while decoding payload: %v\n", err)
		return nil
	}

	return decoded
}

// ------------ Get the node's address ------------

// func GetMyAddress(netAddr string) (string, error) {
// 	if MyIP, err := getMyIP(netAddr); err != nil {
// 		return "", err
// 	} else {
// 		return MyIP.String() + ":" + UDP_PORT, nil
// 	}
// }

// This function attempts to retrieve the network IP we will use
// If no netAddr is provided, it calls getOverlayIP, that will look for an interface w/ MTU==1450 (a Docker network w/ "overlay" driver)
// netAddr must contain a network address in CIDR notation: "x.y.z.t/m"
func GetMyIP(netAddr string) (string, error) {
	if netAddr == "" {
		return getOverlayIP()
	}

	netIP, _, err := net.ParseCIDR(netAddr)
	if err != nil {
		return "", err
	}
	ifaceAddrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}

	for _, ifaceAddr := range ifaceAddrs {
		ifaceIP, ifaceNetIP, err := net.ParseCIDR(ifaceAddr.String())
		//fmt.Fprintf(os.Stderr, "Iface#%v: %v\n", i, ifaceAddr)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error parsing iface's CIDR Address: %v\n", err)
			continue
		}

		if ifaceNetIP.Contains(netIP) {
			fmt.Printf("[getMyIP] We found an IP corresponding to network '%v': %v\n", netAddr, ifaceIP)
			return ifaceIP.String(), nil
		}
	}

	return "", fmt.Errorf("[getMyIP] Error: no interface connected to the network '%v'.\n", netAddr)
}

// This function looks for an interface with MTU=1450:
// It is the interface of Docker's overlay network.
// If getOverlayIP finds the interface, it looks for the external IPv4 to retrieve.
func getOverlayIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}

	var eth net.Interface
	found := false
	for _, iface := range ifaces {
		if iface.MTU == 1450 {
			eth = iface
			found = true
			fmt.Printf("[getOverlayIP] Overlay network found on iface %v\n", iface.Name)
			break
		}
	}
	if !found {
		return "", errors.New("[getOverlayIP] Could not find the overlay network (w/ MTU of 1450).")
	}

	addrs, err := eth.Addrs()
	if err != nil {
		return "", err
	}

	for _, addr := range addrs {
		var ip net.IP
		switch v := addr.(type) {
		case *net.IPNet:
			ip = v.IP
		case *net.IPAddr:
			ip = v.IP
		}
		// I think this is useless
		if ip == nil || ip.IsLoopback() {
			continue
		}
		// Does this just oblige us to use IPv4 or is it useful?
		ip = ip.To4()
		if ip != nil {
			fmt.Printf("[getOverlayIP] Our IP on the overlay is: %v\n", ip)
			return ip.String(), nil
		}
	}
	return "", errors.New("[getOverlayIP] We could not find a valid IPv4 address on the overlay net.")
}
