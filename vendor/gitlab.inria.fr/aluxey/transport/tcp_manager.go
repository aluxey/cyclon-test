package transport

import (
	//"bytes"
	"encoding/gob"
	"fmt"
	//"io"
	"net"
	"os"
	"sync"
)

const (
	TCP_BUF_SIZE    = 10000
	TCP_TIMEOUT     = 2 // seconds
	TCP_LISTEN_PORT = "10337"
	DEBUG_TCP       = false
)

type TCPManager struct {
	observers struct {
		// This map needs a mutex to prevent concurrent use
		sync.RWMutex
		m map[MessageType]chan<- Message
	}

	// The keys of connections are the IPs (not IP:PORT) of the remotes
	connections struct {
		sync.RWMutex
		// For each remote, we keep a connection opened, and the encoder
		// The decoder is tied to the connection created by AcceptTCP, no need to keep it
		mConn map[string]*net.TCPConn
		mEnc  map[string]*gob.Encoder
	}

	ln *net.TCPListener

	myIP    string
	bufSize int

	// handle to stop the daemon
	stopCh chan struct{}
}

func NewTCPManager(myIP string) (*TCPManager, error) {
	nm := new(TCPManager)

	// Creation of the listener on myIP:TCP_LISTEN_PORT
	myTcpAddr, err := net.ResolveTCPAddr("tcp", myIP+":"+TCP_LISTEN_PORT)
	if err != nil {
		return nil, fmt.Errorf(
			"[CreateTCPManager] Failed Resolving my TCP address %q: %v\n",
			myIP+":"+TCP_LISTEN_PORT, err)
	}
	nm.ln, err = net.ListenTCP("tcp", myTcpAddr)
	if err != nil {
		return nil, fmt.Errorf(
			"[CreateTCPManager] Failed initialising TCP Listener at %q: %v\n",
			myTcpAddr, err)
	}

	nm.myIP = myIP
	nm.stopCh = make(chan struct{})
	nm.bufSize = DEFAULT_BUF_SIZE

	nm.observers.m = make(map[MessageType]chan<- Message)

	nm.connections.mConn = make(map[string]*net.TCPConn)
	nm.connections.mEnc = make(map[string]*gob.Encoder)

	go nm.receiveDaemon()

	return nm, nil
}

func (nm *TCPManager) RegisterObserver(messType MessageType, ch chan<- Message) error {
	nm.observers.Lock()
	defer nm.observers.Unlock()

	if nm.observers.m[messType] == nil {
		debugTCP("[TCPManager.RegisterObserver] Observer registered for MessageType %q.\n", messType)
		nm.observers.m[messType] = ch
		return nil
	} else {
		return fmt.Errorf("[TCPManager.RegisterObserver] An observer already exists for MessageType %v.\n", messType)
	}
}

func (nm *TCPManager) Stop() {
	close(nm.stopCh)
	nm.connections.RLock()
	defer nm.connections.RUnlock()
	for _, conn := range nm.connections.mConn {
		conn.Close()
	}
	nm.ln.Close()
}

func (nm *TCPManager) Send(mess Message, recipientIP string) error {
	select {
	case <-nm.stopCh:
		fmt.Fprintf(os.Stderr, "[TCPManager.Send] The network manager is closed.\n")
	default:
		if recipientIP == nm.myIP {

			return fmt.Errorf("You tried to connect to yourself, dumbass")
		}

		var enc *gob.Encoder
		var conn *net.TCPConn
		var ok bool
		var err error
		var recipientTcpAddr *net.TCPAddr

		nm.connections.RLock()
		enc, ok = nm.connections.mEnc[recipientIP]
		nm.connections.RUnlock()

		// Retry until it WORKS
		for {
			// Dialing connection with recipientIP
			if !ok {
				// First resolving address...
				recipientTcpAddr, err = net.ResolveTCPAddr("tcp", recipientIP+":"+TCP_LISTEN_PORT)
				if err != nil {
					return fmt.Errorf(
						"Failed resolving recipient TCP address %q: %v",
						recipientIP+":"+TCP_LISTEN_PORT, err)
				}

				// Creating connection
				conn, err = net.DialTCP("tcp", nil, recipientTcpAddr)
				if err != nil {
					return fmt.Errorf(
						"Failed dialing to %q: %v",
						recipientTcpAddr, err)
				}

				debugTCP("[TCPManager.Send] Created a new connection with %q (\"%v->%v\").\n",
					recipientIP, conn.LocalAddr(), conn.RemoteAddr())

				enc = gob.NewEncoder(conn)

				// Adding to our maps
				nm.connections.Lock()
				nm.connections.mConn[recipientIP] = conn
				nm.connections.mEnc[recipientIP] = enc
				nm.connections.Unlock()

				// Listening the connection
				go nm.listenConnection(conn, recipientIP)
			}

			// Trying to encode the message into the connection
			if err = enc.Encode(&mess); err != nil {
				fmt.Fprintf(os.Stderr,
					"[TCPManager.Send] Failed encoding message %v for %q: %v\n",
					mess.Type, recipientIP, err)
				fmt.Fprintf(os.Stderr, detailError(err))

				fmt.Fprintf(os.Stderr,
					"[TCPManager.Send] Closing connection and retrying.\n")
				// Didn't work? We close the connection and retry
				nm.closeConnection(recipientIP)
				ok = false

				continue
			}

			debugTCP("[TCPManager.Send] Sent %v to %q.\n", mess.Type, recipientIP)

			break
		} // for
	}

	// All is well, return nil
	return nil
}

func (nm *TCPManager) receiveDaemon() {
	for {
		select {
		case <-nm.stopCh:
			break
		default:
			debugTCP("[TCPManager.receiveDaemon] Listening for incoming connections...\n")

			// Listener listens on TCP_LISTEN_PORT
			if conn, err := nm.ln.AcceptTCP(); err != nil {
				fmt.Fprintf(os.Stderr,
					"[TCPManager.receiveDaemon] Failed receiving connection: %v\n", err)
			} else {
				debugTCP(
					"[TCPManager.receiveDaemon] Accepted a new connection (\"%v->%v\").\n",
					conn.LocalAddr(), conn.RemoteAddr())
				// Resolving the remote addr
				recipientTcpAddr, err := net.ResolveTCPAddr("tcp", conn.RemoteAddr().String())
				if err != nil {
					fmt.Fprintf(os.Stderr,
						"[TCPManager.receiveDaemon] Error resolving remote address %q: %v\n",
						conn.RemoteAddr().String(), err)
				}
				recipientIP := recipientTcpAddr.IP.String()

				nm.connections.Lock()
				// We add the new connection to our map if there is no existing one
				if _, ok := nm.connections.mConn[recipientIP]; !ok {

					nm.connections.mConn[recipientIP] = conn
					nm.connections.mEnc[recipientIP] = gob.NewEncoder(conn)
				}
				nm.connections.Unlock()

				go nm.listenConnection(conn, recipientIP)
			}
		}
	}
}

func (nm *TCPManager) listenConnection(conn *net.TCPConn, remoteIP string) {
	// More keepalives are better than less
	conn.SetKeepAlive(true)

	// The decoder exists as long as the connection functions. It will be deleted when listenConnection fails
	dec := gob.NewDecoder(conn)

	// Loops for each new message as long as the connection does not bug
	for {
		select {
		case <-nm.stopCh:
			return
		default:

			debugTCP(
				"[TCPManager.listenConnection] Waiting for messages from %q (\"%v->%v\")...\n",
				remoteIP, conn.LocalAddr(), conn.RemoteAddr())

			var mess Message
			if err := dec.Decode(&mess); err != nil {

				fmt.Fprintf(os.Stderr,
					"[TCPManager.listenConnection] Error while decoding message from %q: %v\n",
					remoteIP, err)
				detailError(err)
				fmt.Fprintf(os.Stderr, detailError(err))

				// Bye bye connection, we won't listen to you again!
				return
			}

			debugTCP("[TCPManager.listenConnection] Received %v from %q (\"%v->%v\").\n",
				mess.Type, remoteIP, conn.LocalAddr(), conn.RemoteAddr())

			nm.observers.RLock()
			if nm.observers.m[mess.Type] != nil {
				nm.observers.m[mess.Type] <- mess
			} else {
				fmt.Fprintf(os.Stderr,
					"[TCPManager.listenConnection] No observer subscribed to %q.\n",
					mess.Type)
			}
			nm.observers.RUnlock()
		}
	}
}

func (nm *TCPManager) closeConnection(recipientIP string) {
	nm.connections.Lock()
	defer nm.connections.Unlock()

	// recipientTcpAddr, _ := net.ResolveTCPAddr("tcp", recipientAddr)
	// recipientIP := recipientTcpAddr.IP.String()

	if conn, ok := nm.connections.mConn[recipientIP]; ok {
		debugTCP("[TCPManager.closeConnection] Closing existing connection with %q...\n", recipientIP)
		if err := conn.Close(); err != nil {
			fmt.Fprintf(os.Stderr, "[TCPManager.closeConnection] Error closing connection with %q: %v\n",
				recipientIP, err)
		}
		delete(nm.connections.mConn, recipientIP)
		delete(nm.connections.mEnc, recipientIP)
	} else {
		debugTCP("[TCPManager.closeConnection] No existing connection with %q.\n", recipientIP)
	}
}

func detailError(err error) (ret string) {
	ret = fmt.Sprintf("ErrString='%s'\nErrorDetails=%#v\n",
		err.Error(), err)
	if oerr, ok := err.(*net.OpError); ok {
		ret += fmt.Sprintf("\tSource=%v\n\tAddr=%v\n\tErrString='%s'\n\tErrDetails=%#v\n",
			oerr.Source, oerr.Addr, oerr.Err.Error(), oerr.Err)

		if serr, ok2 := oerr.Err.(*os.SyscallError); ok2 {
			ret += fmt.Sprintf("\t\tSyscall=%v\n\t\tErrString='%s'\n\t\tErrNo=%d\n",
				serr.Syscall, serr.Err.Error(), serr.Err)
		}
	}
	ret += "\n"
	return
}
func debugTCP(format string, a ...interface{}) {
	if DEBUG_TCP {
		fmt.Printf(format, a...)
	}
}
