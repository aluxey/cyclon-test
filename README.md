# Cyclon-test - My pipeline to test Cyclon 

This code is primarily a check to see if my [cyclon](https://gitlab.inria.fr/aluxey/gossip/tree/master/cyclon) package worked.

Its second goal is to demonstrate how I use my packages to make network simulations using Docker, and how I parse CSV outputs into cool graphs.

It consists of:

* a main Go package (`main.go`);
* a shell script to compile my Go project in a Docker image (`dockerize_go_project.sh`)
* a Python script to launch the experiment (`launch_experiment.py`)
* [Jupyter](https://jupyter.org/) Notebooks: `launch_experiment.ipynb` (that does the same thing as the Python script) and `evaluate_results.ipynb` to parse the CSV output into a figure like `it_works.svg`;
* a `python_modules` folder, containing code to handle Docker, start simulations and fetch the results;
* `it_works.svg`, a figure showing that the algorithm works as expected (using 50 devices, a view size of 10, and a gossip size of 6).

----

By Adrien Luxey in June 2018.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.